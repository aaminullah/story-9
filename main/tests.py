from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from django.contrib.auth.models import User
from django.contrib import auth


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_url(self) :
        response = Client().get("/")
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,"main/home.html")

        response = Client().get("/login")
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,"main/login.html")

    def test_login(self) :
        user = User.objects.create_user('testing', 'testing@testing.com', 'testing8888')
        response = self.client.post("/login", data={'username' : 'testing', 'password' : 'testing8888', 'login':"login"}, follow=True)
        self.assertEqual(response.status_code,200)

        html_response = response.content.decode('utf8')
        self.assertIn('testing', html_response)

    def test_register_url(self) :
        response = Client().get("/signup")
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,"main/signup.html")
    
    def test_register(self) :
        response = self.client.post("/signup", data={'username' : 'testing123123', 'password1' : 'testpassword123','password2' : 'testpassword123', 'signup':"signup"})
        self.assertEqual(response.status_code,302)
        self.assertEqual(1, User.objects.count())
    
    def test_url_logout(self) :
        response = Client().get("/logout")
        self.assertEqual(response.status_code,302)
        user = auth.get_user(self.client)
        self.assertEqual(False, user.is_authenticated)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
