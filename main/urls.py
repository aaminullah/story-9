from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('login', views.user_login, name='login'),
    path('signup', views.user_signUp, name='signup'),
    path('logout', views.logout_user, name='logout'),
]