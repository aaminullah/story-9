from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm



def home(request):
    return render(request, 'main/home.html')


def user_login(request) :
    if request.user.is_authenticated:
        return redirect('main:home')
    if request.method == "POST" :
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('main:home')
        else :
            context = {'failed': 'please check your username or password'}
            return render(request, 'main/login.html', context)
    return render(request, 'main/login.html')

def user_signUp(request) :
    form = UserCreationForm()
    if request.user.is_authenticated:
        return redirect('main:home')

    if request.method == "POST" :
        form = UserCreationForm(request.POST)
        if form.is_valid() :
            form.save()
            new_user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'],)
            login(request, new_user)
            return redirect('main:login')

    context = {'form' : form}
    return render(request, 'main/signup.html', context)

def logout_user(request) :
    logout(request)
    return redirect('main:home')
